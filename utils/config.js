module.exports = {
    url: 'http://114.116.43.111:88/api',
    doubanUrl: 'https://api.douban.com/v2/book/isbn/',
    appKey: '**********',
    appID: 'wx3039184a4ea861e5',
    appSecret: '**********',
    showSuccessTime: 1000,
    clubApi: {
        put: 'https://api.wxappclub.com/put',
        get: 'https://api.wxappclub.com/get',
        del: 'https://api.wxappclub.com/del',
        match: 'https://api.wxappclub.com/match',
        list: 'https://api.wxappclub.com/list',
        wxUser: 'https://api.wxappclub.com/wxUser'
    },
    // 面试类型就三种1：线下，2：电话，3视频
    interviewType: {
        1: '线下',
        2: '电话',
        3: '视频'
    },

    // userStatus   0：未就业，1:已就业
    userStatus: {
        0: '未就业',
        1: '已就业',
        2: '未知'
    },

    // 手机号验证
    telReg: /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/,

    // 身份证号
    idcardReg: /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|30|31)|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}([0-9]|x|X)$/,

    // 所属角色 1.校长 2。主任 3.讲师 4.辅导员 5.学生
    userRole: {
        1: '校长',
        2: '主任',
        3: '讲师',
        4: '辅导员',
        5: '学生'
    },

    aliyun: 'https://jiyundashuju.oss-cn-hangzhou.aliyuncs.com/complaint/'
}