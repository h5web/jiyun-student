//api接口列表
module.exports = {
    "getSmsCode": "/user/code",
    "login": "/user/login",
    "list": "/home/data",
    "category": "/cate/a",
    "home": "/complaint/student/index",
    "detail": "/student/interview/findById",
    "saveInterview": "/student/interview/save",
    "updateInterview": "/student/interview/update",
    "interviewQuestion": "/student/interviewQuestion/findByCondition",
    "getInterviewDetail": "/student/interviewQuestion/findById",
    "returnSchool": "/student/returnSchool/save",
    "recodingList": "/student/interview/recodingList",
    "collect": "/student/studentDirectory/save",
    "delete": "/student/studentDirectory/delete",
    "collectList": "/complaint/student/queryCollection",
    "typeList": "/student/interviewQuestion/findQuestionType",
    "positionList": "/student/usianJob/list",
    "majorList": "/student/returnSchool/findCollege"
}