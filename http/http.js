const evn = require('../env/index.js')
const api = require('./api')
const fetch = require('./fetch')

//确定开发环境
let baseUrl = evn.devBaseUrl

//获取验证码
function getSmsCode(params) {
    return fetch(baseUrl + api.getSmsCode, 'post', params, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

//登陆
function login(obj) {
    return fetch(baseUrl + api.login, 'post', obj, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

//list列表函数
function list(obj) {
    return fetch(baseUrl + api.list, 'get', obj)
}

//首页数据
function home(obj) {
    return fetch(baseUrl + api.home, 'get', obj)
}

//面试详情
function detail(obj) {
    return fetch(baseUrl + api.detail, 'get', obj)
}

//面试报备
function saveInterview(obj) {
    return fetch(baseUrl + api.saveInterview, 'post', obj, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

//提交面试详情面试题，面试录音，面试结果信息
function updateInterview(obj) {
    return fetch(baseUrl + api.updateInterview, 'post', obj, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

//面试宝典列表
function interviewQuestion(obj) {
    return fetch(baseUrl + api.interviewQuestion, 'post', obj, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

//面试宝典列表
function getInterviewDetail(obj) {
    return fetch(baseUrl + api.getInterviewDetail, 'get', obj)
}

//返校报备
function returnSchool(obj) {
    return fetch(baseUrl + api.returnSchool, 'post', obj, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

//录音列表查看
function recodingList(obj) {
    return fetch(baseUrl + api.recodingList, 'get', obj)
}

// 收藏
function collect(obj) {
    return fetch(baseUrl + api.collect, 'post', obj, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

// 取消收藏
function cancelCollect(obj) {
    return fetch(baseUrl + api.delete, 'post', obj, {
        'content-type': 'application/x-www-form-urlencoded'
    })
}

// 收藏列表
function collectList(obj) {
    return fetch(baseUrl + api.collectList, 'get', obj)
}

// 问题类型列表
function typeList(obj) {
    return fetch(baseUrl + api.typeList, 'get', obj)
}

// 招聘职位列表
function positionList(obj) {
    return fetch(baseUrl + api.positionList, 'get', obj)
}

// 获取专业方向
function majorList(obj) {
    return fetch(baseUrl + api.majorList, 'get', obj)
}

//分类接口函数
module.exports = {
    getSmsCode,
    login,
    list,
    home,
    detail,
    saveInterview,
    updateInterview,
    interviewQuestion,
    getInterviewDetail,
    returnSchool,
    recodingList,
    collect,
    cancelCollect,
    collectList,
    typeList,
    positionList,
    majorList
}