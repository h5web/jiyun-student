let app = getApp();
let timer = null; //倒计时

Page({
    data: {
        phone: '',
        smsCode: '',
        loading: false, //是否正在请求中
        timerRuning: false, //是否倒计时进行中
        smsCodeTip: '验证码',
        ischecked: false
    },

    // 选择用户协议
    checked() {
        this.setData({
            ischecked: !this.data.ischecked
        })
    },

    countDown() {
        //60s倒计时
        this.setData({
            timerRuning: true
        })

        let count = 60
        let timer = setInterval(() => {
            if (count == 0) {
                count = 60
                clearInterval(timer)
                this.setData({
                    timerRuning: false,
                    smsCodeTip: '重新获取'
                })
            } else {
                count--
                this.setData({
                    smsCodeTip: count + 's'
                })
            }
        }, 1000)
    },

    phoneChange(e) {
        this.setData({
            phone: e.detail.value.trim()
        })
    },

    smsCodeChange(e) {
        this.setData({
            smsCode: e.detail.value.trim()
        })
    },

    getSmsCode() {
        if (this.data.loading || this.data.timerRuning) {
            return
        }

        let phone = this.data.phone
        if (!phone || !app.config.telReg.test(phone)) {
            wx.showToast({
                title: '请输入手机号',
                icon: 'none'
            })
            return
        }

        this.setData({
            loading: true
        })

        app.http.getSmsCode({
            phone
        }).then(res => {
            res = res.data
            this.setData({
                loading: false
            })

            if (res.code != 200) {
                wx.showModal({
                    content: res.msg,
                    showCancel: false
                })
            } else {
                this.countDown()
            }
        }).catch(res => {
            this.setData({
                loading: false
            })

            wx.showModal({
                content: res.msg,
                showCancel: false
            })
        })
    },

    login() {
        if (!this.data.ischecked) {
            wx.showToast({
                title: '请阅读并勾选用户协议和隐私政策',
                icon: 'none'
            })
            return
        }

        let phone = this.data.phone
        let smsCode = this.data.smsCode

        if (!phone || !app.config.telReg.test(phone)) {
            wx.showToast({
                title: '请输入手机号',
                icon: 'none'
            })
            return
        }

        if (!smsCode) {
            wx.showToast({
                title: '请输入验证码',
                icon: 'none'
            })
            return
        }

        wx.showLoading({
            title: '正在登录...'
        })
        app.http.login({
            phone,
            smsCode
        }).then(res => {
            wx.hideLoading()
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            wx.setStorageSync('user', res.data)
            wx.reLaunch({
              url: '/pages/home/home'
            })
        })
    },

    // 用户协议
    watchAgreement() {
        wx.navigateTo({
            url: '/pages/agreement/agreement'
        })
    },

    // 隐私政策
    watchPrivacy() {
        wx.navigateTo({
            url: '/pages/privacy/privacy'
        })
    },

    onLoad(){
        let user = wx.getStorageSync('user')
        if(user){
            wx.reLaunch({
              url: '/pages/home/home',
            })
        }
    }
});