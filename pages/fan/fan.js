let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userName: '',
        userId: '',
        majorNameSource: [],
        majorName: [],
        majorNameIndex: '',
        identityId: '', //身份证号
        graduationTime: '',
        returnTime: '',
        parentPhone: '',
        telephone: '',
        remark: ''
    },

    getMajorList(){
        app.http.majorList().then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            this.setData({
                majorNameSource: res.data,
                majorName: res.data.map(item => item.cname)
            })
        })
    },

    getTyepList(params) {
        app.http.typeList(params).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            this.setData({
                typeList: res.data
            })
        })
    },

    majorNameChange(e) {
        this.setData({
            majorNameIndex: e.detail.value.trim()
        })
    },

    identityIdChange(e) {
        this.setData({
            identityId: e.detail.value.trim()
        })
    },

    graduationTimeChange: function (e) {
        this.setData({
            graduationTime: e.detail.value.trim()
        })
    },

    returnTimeChange: function (e) {
        this.setData({
            returnTime: e.detail.value.trim()
        })
    },

    parentPhoneChange(e) {
        this.setData({
            parentPhone: e.detail.value.trim()
        })
    },

    telephoneChange(e) {
        this.setData({
            telephone: e.detail.value.trim()
        })
    },

    remarkChange(e) {
        this.setData({
            remark: e.detail.value.trim()
        })
    },

    back() {
        wx.navigateBack({
            delta: 0,
        })
    },

    submit() {
        let userId = this.data.userId
        let majorName = this.data.majorName
        let majorNameIndex = this.data.majorNameIndex
        let identityId = this.data.identityId
        let graduationTime = this.data.graduationTime
        let returnTime = this.data.returnTime
        let parentPhone = this.data.parentPhone
        let telephone = this.data.telephone
        let remark = this.data.remark

        if (!majorName[majorNameIndex] || !graduationTime || !returnTime || !remark) {
            wx.showToast({
                title: '请完善信息后再提交',
                icon: 'none'
            })
            return
        }

        if (!app.config.idcardReg.test(identityId)) {
            wx.showToast({
                title: '请填写正确的身份证号码',
                icon: 'none'
            })
            return
        }

        if (!app.config.telReg.test(parentPhone)) {
            wx.showToast({
                title: '请填写正确的家长电话',
                icon: 'none'
            })
            return
        }

        if (!app.config.telReg.test(telephone)) {
            wx.showToast({
                title: '请填写正确的本人电话',
                icon: 'none'
            })
            return
        }


        let params = {
            userId,
            majorName: majorName[majorNameIndex],
            identityId,
            graduationTime: new Date(graduationTime),
            returnTime: new Date(returnTime),
            parentPhone,
            telephone,
            remark
        }
        app.http.returnSchool(params).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            //成功提醒并返回上一页
            let ths = this
            wx.showToast({
                title: res.msg,
                icon: 'none'
            })
            this.timer = setTimeout(() => {
                ths.back()
            }, 2000);
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let user = wx.getStorageSync('user')
        this.setData({
            userName: (user && user.userName) || '暂无数据',
            userId: (user && user.id) || ''
        })

        this.getMajorList()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        clearTimeout(this.timer)
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})