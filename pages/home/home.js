let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        user: null, //登录用户
        userRole: app.config.userRole,
        detail: null, //首页数据
        config: app.config,
        userStatus: 2, //userStatus   0：未就业，1:已就业, 2: 未知
    },

    initHome() {
        let user = wx.getStorageSync('user')
        this.setData({user})

        app.http.home({
            studentId: user.id
        }).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showModal({
                    content: res.msg,
                    showCancel: false
                })
                return
            }

            let detail = res.data
            if(detail.historyInterview.length>0){
                detail.historyInterview = detail.historyInterview.map(item => {
                    if(item.money){
                        item.money = (Number(item.money)/1000).toFixed(0)
                    }
                    return item
                })
            }

            if(detail.todayInterview.length>0){
                detail.todayInterview = detail.todayInterview.map(item => {
                    if(item.money){
                        item.money = (Number(item.money)/1000).toFixed(0)
                    }
                    return item
                })
            }

            if(detail.tomorrowInterview.length>0){
                detail.tomorrowInterview = detail.tomorrowInterview.map(item => {
                    if(item.money){
                        item.money = (Number(item.money)/1000).toFixed(0)
                    }
                    return item
                })
            }

            this.setData({
                detail
            })
        })
    },

    getUserStatus() {
        let user = wx.getStorageSync('user')
        let userStatus = user ? user.userStatus : 2
        this.setData({
            userStatus
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getUserStatus()
        this.initHome()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})