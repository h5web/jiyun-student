let app = getApp();
let interviewType = Object.keys(app.config.interviewType)
Page({

  /**
   * 页面的初始数据
   */
  data: {
    companyName: '',
    positionSource: [],
    position: [],
    positionIndex: '',
    money: '',
    companyAddress: '',
    date: '年月日',
    time: '时分',
    items: interviewType.map(item => {
      return {
        value: item,
        name: app.config.interviewType[item]
      }
    }),
    hrName: '',
    hrPhone: ''
  },

  back() {
    wx.navigateBack({
      delta: 0,
    })
  },

  companyNameChange(e) {
    this.setData({
      companyName: e.detail.value.trim()
    })
  },

  positionChange(e) {
    this.setData({
      positionIndex: e.detail.value.trim()
    })
  },

  moneyChange(e) {
    this.setData({
      money: e.detail.value.trim()
    })
  },

  companyAddressChange(e) {
    this.setData({
      companyAddress: e.detail.value.trim()
    })
  },

  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },

  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },

  radioChange(e) {
    const items = this.data.items
    for (let i = 0, len = items.length; i < len; ++i) {
      items[i]['checked'] = items[i].value === e.detail.value
    }

    this.setData({
      items
    })
  },

  hrNameChange(e) {
    this.setData({
      hrName: e.detail.value.trim()
    })
  },

  hrPhoneChange(e) {
    this.setData({
      hrPhone: e.detail.value.trim()
    })
  },

  submit() {
    let companyName = this.data.companyName
    let jId = this.data.positionIndex ? this.data.positionSource[this.data.positionIndex].id : ''
    let money = this.data.money
    let companyAddress = this.data.companyAddress
    let date = this.data.date == '年月日' ? '' : this.data.date
    let time = this.data.time == '时分' ? '' : this.data.time
    let interviewTime = new Date(date + ' ' + time)
    let interviewType = this.data.items.filter(item => item.checked)
    interviewType = interviewType.length > 0 ? interviewType[0].value : ''
    let hrName = this.data.hrName
    let hrPhone = this.data.hrPhone
    let sId = wx.getStorageSync('user').id

    if (!companyName || !jId || !money || !companyAddress || !date || !time || !interviewType ||
      !hrName || !hrPhone) {
      wx.showToast({
        title: '请完善后再提交',
        icon: 'none'
      })
      return
    }

    let params = {
      companyName,
      jId,
      money,
      companyAddress,
      interviewTime,
      interviewType,
      hrName,
      hrPhone,
      sId
    }
    app.http.saveInterview(params).then(res => {
      res = res.data

      // 错误提醒不跳转
      if (res.code != 200) {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
        return
      }

      // 成功提醒并跳转到首页
      wx.showToast({
        title: res.msg,
        icon: 'none'
      })

      this.timer = setTimeout(() => {
        wx.navigateTo({
          url: '/pages/home/home'
        })
      }, 2000);
    })
  },

  positionList(){
    app.http.positionList().then(res => {
        res = res.data
        if (res.code != 200) {
            wx.showToast({
                title: res.msg,
                icon: 'none'
            })
            return
        }

        this.setData({
            positionSource: res.data,
            position: res.data.map(item => item.jname)
        })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.positionList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearTimeout(this.timer)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})