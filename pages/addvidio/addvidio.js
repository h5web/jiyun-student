const evn = require('../../env/index.js')
let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        positionSource: [],
        position: [],
        positionIndex: '',
        date: '年月日',
        time: '时分',
        audioUrl: '', //面试录音
    },

    positionList() {
        app.http.positionList().then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            this.setData({
                positionSource: res.data,
                position: res.data.map(item => item.jname)
            })
        })
    },

    positionChange(e) {
        this.setData({
            positionIndex: e.detail.value.trim()
        })
    },

    bindDateChange: function (e) {
        this.setData({
            date: e.detail.value
        })
    },

    bindTimeChange: function (e) {
        this.setData({
            time: e.detail.value
        })
    },

    uploadAudio() {
        let ths = this
        let user = wx.getStorageSync('user')
        let token = user.token
        
        wx.chooseMessageFile({
            count: 1,
            type: 'file',
            extension: ['mp3', 'wav', 'aac', 'm4a'],
            success(res) {
                const tempFilePaths = res.tempFiles
                wx.uploadFile({
                    url: evn.devBaseUrl + '/complaint/upload',
                    filePath: tempFilePaths[0].path,
                    header: {token},
                    name: 'file',
                    success(res) {
                        const data = res.data
                        console.log('success', data)
                    },
                    fail(res) {
                        console.log('fail', res)
                    },
                    complete(res) {
                        console.log('complete', res)
                    }
                })
            }
        })
    },

    submit() {
        let jId = this.data.positionIndex ? this.data.positionIndex - 1 + 2 : ''
        let date = this.data.date == '年月日' ? '' : this.data.date
        let time = this.data.time == '时分' ? '' : this.data.time
        let interviewTime = date + ' ' + time
        interviewTime = new Date(interviewTime)
        let fileUrl = this.data.audioUrl

        if(!jId || !date || !time || !fileUrl){
            wx.showToast({
              title: '请完善信息后再提交',
              icon: 'none'
            })
            return
        }

        let params = {jId, interviewTime, fileUrl}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.positionList()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})