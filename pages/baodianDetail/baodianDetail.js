let app = getApp();
let {
    wxParse
} = require('../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        detail: null
    },

    getInterviewDetail(params) {
        app.http.getInterviewDetail(params).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            this.setData({
                detail: res.data
            })

            let datas = res.data.content;
            wxParse('datas', 'html', datas, this, 0);
        })
    },

    dianzan() {
        let stuId = wx.getStorageSync('user').id
        let dirId = this.data.detail.id

        app.http.collect({
            stuId,
            dirId
        }).then(res => {
            res = res.data

            if (res.code == 200) {
                let detail = this.data.detail
                detail.isCollection = true
                detail.count += 1
                this.setData({
                    detail
                })
            } else {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
            }
        })
    },

    cancelDianzan() {
        let stuId = wx.getStorageSync('user').id
        let dirId = this.data.detail.id

        app.http.cancelCollect({
            stuId,
            dirId
        }).then(res => {
            res = res.data

            if (res.code == 200) {
                let detail = this.data.detail
                detail.isCollection = false
                detail.count -= 1
                this.setData({
                    detail
                })
            } else {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let id = options.id || 1
        let stuId = wx.getStorageSync('user').id
        this.getInterviewDetail({
            id,
            stuId
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})