let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        datalist: []
    },

    init() {
        let user = wx.getStorageSync('user')

        app.http.collectList({
            studentId: user.id
        }).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showModal({
                    content: res.msg,
                    showCancel: false
                })
                return
            }
            this.setData({
                datalist: res.data
            })
        })
    },

    GoDetails(e) {
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '/pages/baodianDetail/baodianDetail?id=' + id
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.init()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})