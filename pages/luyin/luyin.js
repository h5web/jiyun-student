let app = getApp();
let innerAudioContext = null //播放器
const fs = wx.getFileSystemManager()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list: [],
        page: 1,
        uid: 0
    },

    back() {
        wx.navigateBack({
            delta: 0,
        })
    },

    recodingList() {
        let sid = this.data.uid
        let page = this.data.page
        let params = {
            sid,
            page
        }
        app.http.recodingList(params).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            this.setData({
                list: res.data
            })
        })
    },

    downRecord(e) {
        let filepath = e.currentTarget.dataset.filepath
        let filename = filepath.replace(app.config.aliyun, '')
        let localFilePath = wx.env.USER_DATA_PATH + '/' + filename
        let ths = this

        if (!filepath) {
            wx.showToast({
                title: '录音文件缺失',
                icon: 'none'
            })
            return
        }

        wx.showLoading({
            title: '下载中...'
        })

        // 如果本地有录音缓存直接播放，否则下载完再播放
        if (wx.getStorageSync(localFilePath)) {
            wx.hideLoading()
            this.play(localFilePath)
            return
        }

        wx.downloadFile({
            url: filepath,
            filePath: localFilePath,
            timeout: 1000 * 60 * 5,
            success(res) {
                wx.setStorageSync(localFilePath, true)
                if (res.statusCode == 200) {
                    ths.play(localFilePath)
                }
            },
            fail(res) {
                wx.showToast({
                    title: res.errMsg,
                    icon: 'none'
                })
            },
            complete() {
                wx.hideLoading()
            }
        })
    },

    play(localFilePath) {
        if (!innerAudioContext) {
            innerAudioContext = wx.createInnerAudioContext()
        }
        innerAudioContext.src = localFilePath
        innerAudioContext.play()
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let user = wx.getStorageSync('user')
        let uid = user ? user.id : ''
        this.setData({
            uid
        })
        this.recodingList()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        if (innerAudioContext) {
            innerAudioContext.destroy()
        }
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.setData({
            page: 1
        })
        this.recodingList()
        wx.stopPullDownRefresh()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.setData({
            page: this.data.page + 1
        })
        this.recodingList()
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})