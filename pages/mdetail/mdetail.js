let app = getApp()
const evn = require('../../env/index')

Page({

    /**
     * 页面的初始数据
     */
    data: {
        detail: null,
        config: app.config,
        question: '', //面试题,
        audioUrl: '', //面试录音
        results: [{
                value: 1,
                name: '已下offer',
                checked: false
            },
            {
                value: 2,
                name: '待复试',
                checked: false
            },
            {
                value: 3,
                name: '等通知',
                checked: false
            },
            {
                value: 4,
                name: '不合适',
                checked: false
            },
        ]
    },

    radioChange(e) {
        const results = this.data.results
        for (let i = 0, len = results.length; i < len; ++i) {
            results[i].checked = results[i].value == e.detail.value
        }

        this.setData({
            results
        })
    },

    questionChange(e) {
        let question = e.detail.value.trim()
        this.setData({
            question
        })
    },

    back() {
        wx.navigateBack({
            delta: 0,
        })
    },

    uploadAudio() {
        let ths = this
        let user = wx.getStorageSync('user')
        let token = user.token

        wx.chooseMessageFile({
            count: 1,
            type: 'file',
            extension: ['mp3', 'wav', 'aac', 'm4a'],
            success(res) {
                const tempFilePaths = res.tempFiles
                wx.showLoading({
                  title: '上传中...',
                })
                wx.uploadFile({
                    url: evn.devBaseUrl + '/complaint/upload',
                    filePath: tempFilePaths[0].path,
                    name: 'file',
                    header: {token},
                    timeout: 1000*60*5,
                    success(res) {
                        res = JSON.parse(res.data)
                        if(res.code != 200){
                            wx.showToast({
                              title: res.msg,
                              icon: 'none'
                            })
                            return
                        }
                      
                        ths.setData({
                            audioUrl: res.data
                        })
                    },
                    fail(res) {
                        wx.showToast({
                            title: res.errMsg,
                            icon: 'none'
                          })
                    },
                    complete(res) {
                        wx.hideLoading()
                    }
                })
            }
        })
    },

    updateInterview() {
        let checkedResult = this.data.results.filter(item => item.checked)
        if(checkedResult.length <= 0){
            wx.showToast({
              title: '请选择面试结果后再提交',
              icon: 'none'
            })
            return
        }

        let params = {
            id: this.data.detail.id,
            difficultProblem: this.data.question,
            fileUrl: this.data.audioUrl,
            interviewResult: checkedResult[0].name
        }

        app.http.updateInterview(params).then(res => {
            res = res.data
            wx.showToast({
                title: res.msg,
                icon: 'none'
            })

            if(res.code == 200){
                setTimeout(()=>{
                    wx.navigateBack()
                }, 2000)
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let id = options.id
        app.http.detail({
            id
        }).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            let detail = res.data
            detail['formatMoney'] = (Number(detail.money)/1000).toFixed(0)

            let results = this.data.results.map(item => {
                if(item.name == detail.interviewResult){
                    item.checked = true
                }
                return item
            })
            
            this.setData({
                detail,
                question: detail.difficultProblem,
                audioUrl: detail.fileUrl,
                results
            })
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})