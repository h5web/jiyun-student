let app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        questions: [],
        type: '', //问题类型
        collegeId: '', //专业方向id
        title: '', //搜索的关键字
        page: 1,
        typeList: [], //问题类型列表
    },

    search(value) {
        this.setData({
            title: value.trim(),
            page: 1
        })

        this.getInterviewQuestion({
            type: this.data.type,
            collegeId: this.data.collegeId,
            title: this.data.title,
            page: this.data.page
        })
    },

    clearInput(){
        this.setData({
            title: '',
            page: 1
        })

        this.getInterviewQuestion({
            type: this.data.type,
            collegeId: this.data.collegeId,
            title: this.data.title,
            page: this.data.page
        })
    },

    GoDetails(e) {
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '/pages/baodianDetail/baodianDetail?id=' + id
        })
    },

    getInterviewQuestion(params) {
        app.http.interviewQuestion(params).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            this.setData({
                questions: this.data.page == 1 ? res.data.records : [...this.data.questions, ...res.data.records]
            })

            console.log(this.data.questions)
        })
    },

    getTyepList(params) {
        app.http.typeList(params).then(res => {
            res = res.data
            if (res.code != 200) {
                wx.showToast({
                    title: res.msg,
                    icon: 'none'
                })
                return
            }

            this.setData({
                typeList: res.data
            })
        })
    },

    typelistChange(e) {
        let type = e.currentTarget.dataset.type
        this.setData({
            type
        })

        this.setData({
            page: 1
        })

        this.getInterviewQuestion({
            type: this.data.type,
            collegeId: this.data.collegeId,
            title: this.data.title,
            page: this.data.page
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let user = wx.getStorageSync('user')
        this.setData({
            collegeId: user.cid,
            search: this.search.bind(this)
        })

        this.getTyepList()

        this.getInterviewQuestion({
            type: this.data.type,
            collegeId: this.data.collegeId,
            title: this.data.title,
            page: this.data.page
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },
    onShow: function () {},
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.setData({
            page: 1
        })

        this.getInterviewQuestion({
            type: this.data.type,
            collegeId: this.data.collegeId,
            title: this.data.title,
            page: this.data.page
        })

        wx.stopPullDownRefresh()
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.setData({
            page: this.data.page + 1
        })

        this.getInterviewQuestion({
            type: this.data.type,
            collegeId: this.data.collegeId,
            title: this.data.title,
            page: this.data.page
        })
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})