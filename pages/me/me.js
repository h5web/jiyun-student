
//获取应用实例
let app = getApp()

Page({
    data: {
        userInfo: null,
        showBadge: false,
        meList: [{
                text: '录音列表',
                icon: '../../assets/img/iconfont-dingdan.png',
                url: '../luyin/luyin'
            },
            // {
            //     text: '面试详情',
            //     icon: '../../assets/img/iconfont-help.png',
            //     url: '../mdetail/mdetail'
            // },
            // {
            //     text: '上传录音',
            //     icon: '../../assets/img/iconfont-kefu.png',
            //     url: '../addvidio/addvidio'
            // },
            {
                text: '返校报备',
                icon: '../../assets/img/iconfont-tuihuo.png',
                url: '../fan/fan'
            },
        ]
    },

    onLoad: function () {
        this.setData({
            userInfo: app.globalData.userInfo
        })
    }
})